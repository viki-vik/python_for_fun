########################################################
# created by:   Viki Baktov
# purpose:      AZLyrics Scrapper
# date:         3/07/21
# version:      1.0
########################################################
from bs4 import BeautifulSoup, NavigableString, Tag
import urllib.request


# function gets a song and artist name and returns the song lyrics
def get_song_lyrics(artist='', song=''):
    url = "https://www.azlyrics.com/lyrics/"
    url = url + artist + "/" + song + ".html"
    lyrics = ''
    source = urllib.request.urlopen(url)
    soup = BeautifulSoup(source, 'html.parser')
    # extract title of the song
    for title in soup.findAll('title'):
        lyrics = title.text.strip()
    # extract lyrics
    for br in soup.findAll('br'):
        next_s = br.nextSibling
        if not (next_s and isinstance(next_s, NavigableString)):
            continue
        next2_s = next_s.nextSibling
        if next2_s and isinstance(next2_s, Tag) and next2_s.name == 'br':
            text = str(next_s).strip()
            if text:
                lyrics = lyrics + next_s
    return lyrics


if __name__ == '__main__':
    print(get_song_lyrics('linkinpark', 'battlesymphony'))


