########################################################
# created by:   Viki Baktov
# purpose:      filenames manipulation
# date:         3/07/21
# version:      1.0
########################################################
import shutil
import os
from detect_delimiter import detect
import re, datetime


ZIPPED_DIR = "csv-files.zip"
UNZIPPED_DIR = "CSV-Files"
# the archive file
filename = ZIPPED_DIR

# Unpack the archived file
shutil.unpack_archive(filename)
directories = os.listdir(UNZIPPED_DIR)

# function adds the file date (within the file name) as a new column to the actual file
def add_fileline_column():
    for file in directories:
        file_name = os.path.splitext(file)[0]
        date_extract = re.sub(r'([_]*Test_file_name_[0-9]*[_]*)', "", file_name)
        # print(datetime.datetime.strptime(date_extract[0:7], "%m%d%Y"))
        date_extract = re.sub(r'([_]*[-]*)', "", date_extract)
        with open(os.path.join(UNZIPPED_DIR,file)) as input_file:
            with open('output_file_' + file, 'w+') as output_file:
                for line in input_file.readlines():
                    # print(line)
                    delimiter = detect(line)
                    output_file.write(line.strip() + delimiter[0] + date_extract[0:8] + '\n')




