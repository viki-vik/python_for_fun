# Filenames & columns manipulation
Function which adds the file date (within the file name) as a new column to the
actual file. Note that each file has a different delimiter so you will need to detect the delimiter
automatically.
###### DATE FORMAT
The date format should be - yyyyMMddhhmmss
###### WORKDIR
Input files - csv-files.rar ( unzip first )
###### EXAMPLES
1. Test_file_name_1_2019-05-20_000400.csv
Output:
aaaaa|bbbbb|bbbbbbb|c111|20190520000400
aaaaa|bb234bb|bbffbb|c55c|20190520000400
aaaaa|bb4bb|bbgabbb|cc55|20190520000400
2. Test_file_name_2_20190520.csv
Output:
aaaaa^bbbbb^bbbbbbb^c111^20190520000000
aaaaa^bb234bb^bbffbb^c55c^20190520000000
aaaaa^bb4bb^bbgabbb^cc55^20190520000000
3. 05202019_Test_file_name_3.csv
Output:
aaaaa,bbbbb,bbbbbbb,c111,20190520000000
