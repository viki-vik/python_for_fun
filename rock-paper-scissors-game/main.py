###############################################################
# created by:   Viki Baktov
# purpose:      Rock-Paper-Scissors competition implementation 
# date:         3/07/21
# version:      1.0
###############################################################

from game_functions import get_player_name, get_player_choice, determine_winner, Choice
import shutil

if __name__ == '__main__':
    player1_name = ''
    player2_name = ''
    round_counter = 0
    player1_victories = 0
    player2_victories = 0
    game_records = {'round_record': []}

    while True:
        try:
            player1_name = get_player_name()
            player2_name = get_player_name()
        except ValueError:
            print('Error!!! Please insert your name!!! Try again')
        break
    while True:
        try:
            player1_choice = get_player_choice()
            player2_choice = get_player_choice()
        except ValueError:
            range_str = f"[0, {len(Choice) - 1}]"
            print(f"Invalid selection. Enter a value in range {range_str}")
            continue

        winner = determine_winner(player1_choice, player1_name, player2_choice, player2_name)
        round_counter += 1
        game_records['round_record'].append({
            "round": round_counter,
            "winner": winner,
            "player1_name": player1_name,
            "player2_name": player2_name,
            "player1_weapon": str(Choice(player1_choice)).replace("Choice.", ""),
            "player2_weapon": str(Choice(player2_choice)).replace("Choice.", "")
        })

        play_again = input("Play again? (y/n): ")
        if play_again.lower() != "y":
            break
    # count victories by player
    for g in game_records["round_record"]:
        if g.get('winner') == player1_name:
            player1_victories += 1
        else:
            player2_victories += 1
    # get weapons used in game by player
    player1_weapons = []
    for g in game_records["round_record"]:
        player1_weapons.append(g.get('player1_weapon'))
    player2_weapons = []
    for p in game_records["round_record"]:
        player2_weapons.append(p.get('player2_weapon'))
    # determine the game winner
    if player1_victories > player1_victories:
        game_winner = player1_name
    elif player1_victories < player1_victories:
        game_winner = player2_name
    else:
        game_winner = "NO ONE"
    # print game results report
    columns = shutil.get_terminal_size().columns
    print("###GAME RESULTS###".center(columns))
    print(f"The winner is {game_winner} !!!")
    print(f"All in all there were {round_counter} rounds")
    print(f"{player1_name} won {player1_victories} rounds, and {player2_name} won {player2_victories} rounds.")
    for g in game_records["round_record"]:
        print('Round', str(g['round']), ' : ', f"{g['winner']:8s}", ' won - ', f"{player1_name:8s}", ' had ',
              g['player1_weapon'][:1], ' and ', f"{player2_name:8s}", ' had ', g['player2_weapon'][:1], '.')
    for c in Choice:
        print('The weapon', f"{c.name:10s}", 'was used', f"{str(player1_weapons.count(c.name)):3s}",
              'times by', f"{player1_name:8s}", 'and', f"{str(player2_weapons.count(c.name)):3s}", 'times by'
              , f"{player2_name:8s}")
    print("###END REPORT###".center(columns))


