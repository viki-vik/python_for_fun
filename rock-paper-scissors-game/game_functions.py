###############################################################
# Functions for Rock-Paper-Scissors competition implementation
###############################################################
from enum import IntEnum


class Choice(IntEnum):
    Rock = 0
    Paper = 1
    Scissors = 2


def get_player_name():
    player_name = input('Please insert your name: ')
    return player_name


def get_player_choice():
    choices = [f"{c.name}[{c.value}]" for c in Choice]
    choices_str = ", ".join(choices)
    selection = input(f"Enter a choice ({choices_str}): ")
    choice = Choice(int(selection))
    return choice


def determine_winner(player_choice1, player1, player_choice2, player2):
    victories = {
        Choice.Rock: [Choice.Scissors],  # Rock beats scissors
        Choice.Paper: [Choice.Rock],     # Paper beats rock
        Choice.Scissors: [Choice.Paper]  # Scissors beats paper
    }

    defeats = victories[player_choice1]
    if player_choice1 == player_choice2:
        print(f"Both players selected {player_choice1.name}. It's a tie!")
        winner = "No one"
    elif player_choice2 in defeats:
        print(f"{player_choice1.name} beats {player_choice2.name}! {player1} win!")
        winner = player1
    else:
        print(f"{player_choice2.name} beats {player_choice1.name}! {player2} win!")
        winner = player2
    return winner
