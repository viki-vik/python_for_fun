# Using REGEX
- Function which replaces the file delimiter from comma (,) to pipe (|).
- In case of a value surrounded by double quotes, the code should only remove the double quotes and keep the comma as a value.

###### WORKDIR
Input file - text_files.rar
Output file - ${original_file_name}.pipe

###### Examples:
A,b,aaaa,vvvv -&gt; A|b|aaaa|vvvv
A,3123,&quot;sdasd,a&quot;,2230 -&gt; A|3123|sdasd,a|2230
