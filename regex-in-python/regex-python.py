########################################################
# created by:   Viki Baktov
# purpose:      REGEX practice
# date:         3/07/21
# version:      1.0
########################################################
import re


# function which replaces the file delimiter from comma (,) to pipe (|)
def replace_delimiter(delimiter):
    with open('Test_Comma_To_Pipe.txt') as input_file:
        with open('Test_Comma_To_Pipe.pipe', 'w+') as output_file:
            for line in input_file.readlines():
                l1 = re.sub(r',(?=[^"]*"(?:[^"]*"[^"]*")*[^"]*$)', ">##<", line)
                l2 = l1.replace(delimiter, '|')
                l3 = l2.replace('"', '')
                # extra step removing multiple comma from pattern in quotes
                l4 = re.sub(r'(>##<)+', ",", l3)
                output_file.write(l4)
